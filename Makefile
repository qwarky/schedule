_tests:
	@pytest schedule/ --cov --cov-fail-under=100 --cov-report=term-missing

tests:
	@docker-compose run --rm -w /app web make _tests

checks:
	@docker-compose run --rm -w /app web './checks.sh'

bash:
	@docker-compose run --rm -w /app web bash

docker:
	@docker-compose build --pull