#!/bin/bash

# Fail if any check fails, but run them all

stat=0


export DJANGO_SETTINGS_MODULE=schedule.settings

echo "Type checks... (mypy)"
mypy schedule/ --show-error-codes --show-traceback || ((stat++))

echo "Static checks... (pylint)"
pylint schedule || ((stat++))

echo "Imports checks... (isort)"
isort schedule/ --check --diff || ((stat++))

echo "Format checks... (black)"
black schedule/ --check --diff || ((stat++))

echo "Django checks"
python manage.py check || ((stat++))


exit $stat
