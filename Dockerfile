FROM python:3.11-slim

ENV PYTHONUNBUFFERED 1
ENV POETRY_VER=1.3.1

# For command shortcuts defined in Makefile
RUN apt update && apt install -y make

WORKDIR /app

RUN pip install -U pip
RUN pip install poetry==$POETRY_VER

COPY pyproject.toml poetry.lock checks.sh ./
RUN chmod +x ./ checks.sh

RUN poetry config virtualenvs.create false && poetry install --no-root --no-interaction


