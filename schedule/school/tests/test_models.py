"""Schedule models test module"""
import pytest
from model_bakery import baker


@pytest.mark.django_db
def test_schedule_str():
    """test `Schedule` object str representation"""
    lesson = baker.make("school.Schedule")
    week_day_name = lesson.get_day_of_week_display()
    hour_display = lesson.get_hour_display()
    assert (
        str(lesson)
        == f"{lesson.subject} - {lesson.school_class} {week_day_name} {hour_display}"
    )


@pytest.mark.django_db
def test_student_str():
    """test `Student` object str representation"""
    student = baker.make("school.Student")
    assert str(student) == student.name


@pytest.mark.django_db
def test_teacher_str():
    """test `Teacher` object str representation"""
    teacher = baker.make("school.Teacher")
    assert str(teacher) == teacher.name
