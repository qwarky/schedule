"""Schedule views test module"""
import calendar
from datetime import timedelta

import pytest
from django.utils import timezone
from freezegun import freeze_time
from model_bakery import baker
from rest_framework.reverse import reverse


@pytest.mark.django_db
def test_schedule_view(api_client):
    """test schedule view without filters"""
    lessons = baker.make("school.Schedule", _quantity=20)

    response = api_client.get(reverse("schedule-list"))

    assert len(response.json()) == len(lessons)


@pytest.mark.django_db
def test_schedule_view_for_today(api_client):
    """test schedule view for today"""
    for day_number in range(0, 7):
        baker.make("school.Schedule", day_of_week=day_number, _quantity=day_number + 1)

    responses = {}
    for day_shift in range(0, 7):
        with freeze_time(timezone.now() + timedelta(days=day_shift)):
            responses[timezone.now().weekday()] = api_client.get(
                f"{reverse('schedule-list')}?for_today=true"
            )

    for day_number, response in responses.items():
        assert len(response.json()) == day_number + 1
        assert response.json()[0]["day_of_week"] == calendar.day_name[day_number]
