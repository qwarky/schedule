"""Schedule selector test module"""
import pytest
from model_bakery import baker

from schedule.school.selectors.schedule import ScheduleSelector


@pytest.mark.django_db
@pytest.mark.parametrize("day_number", list(range(0, 7)))
def test_get_schedule_for_week_days(day_number):
    """test selector by week days"""
    baker.make("school.Schedule", day_of_week=day_number, _quantity=day_number + 1)

    schedule = ScheduleSelector.get_schedule(day_number=day_number)

    assert schedule.count() == day_number + 1
    assert schedule.first().day_of_week == day_number


@pytest.mark.django_db
def test_get_shedule_for_class_name():
    """test selector by class_name"""
    baker.make("school.Schedule", _quantity=10)
    school_class = baker.make("school.SchoolClass", name="SC_UNIQUE_NAME")
    baker.make("school.Schedule", school_class=school_class)

    schedule_for_all = ScheduleSelector.get_schedule()
    schedule_for_unique_class_name = ScheduleSelector.get_schedule(
        class_name="SC_UNIQUE_NAME"
    )

    assert schedule_for_all.count() == 11
    assert schedule_for_unique_class_name.count() == 1
