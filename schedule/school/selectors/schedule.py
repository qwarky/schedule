"""`ScheduleSelector` module"""
from functools import lru_cache
from typing import Optional

from django.db.models import Count, Prefetch, QuerySet

from schedule.school.models.schedule import Schedule
from schedule.school.models.school_class import SchoolClass


class ScheduleSelector:  # pylint: disable=too-few-public-methods
    """`ScheduleSelector`"""

    @staticmethod
    @lru_cache(maxsize=1000)
    def get_schedule(
        day_number: Optional[int] = None, class_name: Optional[str] = None
    ) -> QuerySet[Schedule]:
        """return lessons queryset parametrized with week_day and class_name"""
        schedule = Schedule.objects.all()  # pylint: disable=maybe-no-member
        if day_number is not None:
            schedule = schedule.filter(day_of_week=day_number)
        if class_name is not None:
            schedule = schedule.filter(school_class__name__iexact=class_name)
        student_count_annotate = (
            SchoolClass.objects.annotate(  # pylint: disable=maybe-no-member
                student_count=Count("students")
            )
        )
        return schedule.select_related("subject__teacher").prefetch_related(
            Prefetch("school_class", queryset=student_count_annotate, to_attr="class")
        )
