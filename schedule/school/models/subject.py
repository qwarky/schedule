"""`Subject` module"""
from django.db import models

from schedule.school.models.teacher import Teacher


class Subject(models.Model):
    """`Subject`"""

    name = models.CharField(max_length=100)
    teacher = models.ForeignKey(Teacher, on_delete=models.SET_NULL, null=True)

    def __str__(self) -> str:
        return f"{self.name}"
