"""`Student` module"""
from django.db import models

from schedule.school.models.school_class import SchoolClass


class Student(models.Model):
    """`Student`"""

    name = models.CharField(max_length=200)
    school_class = models.ForeignKey(
        SchoolClass, on_delete=models.SET_NULL, null=True, related_name="students"
    )

    def __str__(self) -> str:
        return f"{self.name}"
