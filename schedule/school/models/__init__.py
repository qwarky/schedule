"""school models"""
from schedule.school.models.schedule import Schedule
from schedule.school.models.school_class import SchoolClass
from schedule.school.models.student import Student
from schedule.school.models.subject import Subject
from schedule.school.models.teacher import Teacher
