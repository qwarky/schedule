"""`Schedule` module"""
import calendar

from django.db import models

from schedule.school.models.school_class import SchoolClass
from schedule.school.models.subject import Subject


class Schedule(models.Model):
    """`Schedule`"""

    HOURS = [(hour, f"{hour:02}:00") for hour in range(0, 24)]

    class Weekdays(models.IntegerChoices):
        """`Weekdays`"""

        MONDAY = 0, calendar.day_name[0]
        TUESDAY = 1, calendar.day_name[1]
        WEDNESDAY = 2, calendar.day_name[2]
        THURSDAY = 3, calendar.day_name[3]
        FRIDAY = 4, calendar.day_name[4]
        SATURDAY = 5, calendar.day_name[5]
        SUNDAY = 6, calendar.day_name[6]

    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    school_class = models.ForeignKey(SchoolClass, on_delete=models.CASCADE)
    hour = models.PositiveIntegerField(choices=HOURS, db_index=True)
    day_of_week = models.IntegerField(choices=Weekdays.choices, db_index=True)

    class Meta:  # pylint: disable=too-few-public-methods
        """Meta class"""

        constraints = [
            models.UniqueConstraint(
                fields=["school_class", "day_of_week", "hour"],
                name="school_class_unique_lesson",
            )
        ]

    def __str__(self) -> str:
        week_day_name = getattr(self, "get_day_of_week_display")()
        hour_display = getattr(self, "get_hour_display")()
        return f"{self.subject} - {self.school_class} {week_day_name} {hour_display}"
