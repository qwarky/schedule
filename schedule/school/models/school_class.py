"""`SchoolClass` module"""
from django.db import models


class SchoolClass(models.Model):
    """`SchoolClass`"""

    name = models.CharField(max_length=20, db_index=True)

    def __str__(self) -> str:
        return f"{self.name}"
