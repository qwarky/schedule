"""`Teacher` module"""
from django.db import models


class Teacher(models.Model):
    """`Teacher`"""

    name = models.CharField(max_length=200)

    def __str__(self) -> str:
        return f"{self.name}"
