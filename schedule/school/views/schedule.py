"""Schedule API Views module"""
from django.db.models import QuerySet
from django.utils import timezone
from rest_framework.generics import ListAPIView

from schedule.school.selectors.schedule import ScheduleSelector
from schedule.school.serializers.schedule import ScheduleSerializer


class ScheduleListView(ListAPIView):
    """List of all lessons, can be filtered by 'for_today=true' and `class_name` query strings"""

    serializer_class = ScheduleSerializer
    permission_classes = ()

    def get_queryset(self) -> QuerySet:
        day_number = (
            timezone.now().weekday()
            if self.request.query_params.get("for_today") in ["true", "True"]
            else None
        )
        class_name = self.request.query_params.get("class_name", None)
        return ScheduleSelector.get_schedule(
            day_number=day_number, class_name=class_name
        )
