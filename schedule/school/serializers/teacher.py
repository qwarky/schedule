"""`TeacherSerializer` module"""
from rest_framework import serializers

from schedule.school.models.teacher import Teacher


class TeacherSerializer(serializers.ModelSerializer):
    """`TeacherSerializer`"""

    class Meta:  # pylint: disable=too-few-public-methods
        """meta class"""

        model = Teacher
        fields = ("name",)
