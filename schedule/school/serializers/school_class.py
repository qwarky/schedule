"""`SchoolClassSerializer` module"""
from rest_framework import serializers

from schedule.school.models.school_class import SchoolClass


class SchoolClassSerializer(serializers.ModelSerializer):
    """`SchoolClassSerializer`"""

    student_count = serializers.IntegerField(read_only=True)

    class Meta:  # pylint: disable=too-few-public-methods
        """meta class"""

        model = SchoolClass
        fields = ("name", "student_count")
