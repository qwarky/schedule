"""`ScheduleSerializer` module"""
from rest_framework import serializers

from schedule.school.models.schedule import Schedule
from schedule.school.serializers.school_class import SchoolClassSerializer
from schedule.school.serializers.subject import SubjectSerializer


class ScheduleSerializer(serializers.ModelSerializer):
    """`ScheduleSerializer`"""

    school_class = SchoolClassSerializer()
    subject = SubjectSerializer()
    hour = serializers.CharField(source="get_hour_display")
    day_of_week = serializers.CharField(source="get_day_of_week_display")

    class Meta:  # pylint: disable=too-few-public-methods
        """meta class"""

        model = Schedule
        fields = ("school_class", "subject", "hour", "day_of_week")

    def get_fields(self) -> dict:
        defined_fields = super().get_fields()
        school_class = defined_fields.pop("school_class")
        defined_fields["class"] = school_class
        return defined_fields
