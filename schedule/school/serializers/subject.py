"""`SubjectSerializer` module"""
from rest_framework import serializers

from schedule.school.models.subject import Subject
from schedule.school.serializers.teacher import TeacherSerializer


class SubjectSerializer(serializers.ModelSerializer):
    """`SubjectSerializer"""

    teacher = TeacherSerializer()

    class Meta:  # pylint: disable=too-few-public-methods
        """meta class"""

        model = Subject
        fields = ("name", "teacher")
