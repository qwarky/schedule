# School Schedule

## Running app

### Before start
Before first run, we have to create configuration file. Enclosed .env.example should run out of the box.
```
# Creating env from template.
$ cp .env.example .env
```

### Starting and stopping app

To start project 
```
$ docker-compose up -d
```

To stop project 

```
$ docker-compose down
```

## Dependencies

Dependencies are controled by [poetry](https://python-poetry.org/)

### Adding, removing dependencies 

To add dependency

```
$ make bash         # open basha inside docker
$ poetry add ...    # adding dependency
$ exit              # exit container
$ make docker       # rebuild docker container
```

To remove dependency

```
$ make bash         # open basha inside docker
$ poetry remove ...    # adding dependency
$ exit              # exit container
$ make docker       # rebuild docker container
```

## Testing

Test coverage is set to 100%.

Runnings tests

```
make tests
```

## Code QA
Code quality is done by mypy, pylint, isort, black

Automatic code checks 

```
make checks
```

